package com.miaoshaproject.service;

import com.miaoshaproject.service.model.PromoModel;

/**
 * @author ：李世琦
 * @date ：Created in 2019-02-12 10:14
 * @description：
 */
public interface PromoService {
    PromoModel getPromoByItemId(Integer itemId);
}