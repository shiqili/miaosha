package com.miaoshaproject.service;

import com.miaoshaproject.error.BusinessException;
import com.miaoshaproject.service.model.OrderModel;

/**
 * @author ：李世琦
 * @date ：Created in 2019-02-11 16:12
 * @description：
 */
public interface OrderService {
    //1，通过传递秒杀活动id，后端校验活动是否属于该商品，以及活动是否已经开始
    //2，直接在下单接口内判断对应商品是否有正在进行中秒杀活动，然后生成订单

    //第一种比较好，同一个商品可能存在多个秒杀活动，第二种普通商品也去查询活动信息，费资源
    OrderModel createOrder(Integer userId,Integer itemId,Integer promoId,Integer amount) throws BusinessException;
}