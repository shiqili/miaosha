package com.miaoshaproject.service.model;

import java.math.BigDecimal;

/**
 * @author ：李世琦
 * @date ：Created in 2019-02-11 15:38
 * @description：用户下单交易模型
 */
public class OrderModel {
    private String id;

    private Integer userId;

    //商品id
    private Integer itemId;

    //活动id 若非空，则表示以活动商品方式下单
    private  Integer promoId;

    public Integer getPromoId() {
        return promoId;
    }

    public void setPromoId(Integer promoId) {
        this.promoId = promoId;
    }

    //商品单价,若promoId 非空，则表示以秒杀商品价格下单
    private BigDecimal itemPrice;

    //数量
    private Integer amount;

    //金额，若promoId 非空，则表示以秒杀商品价格下单
    private BigDecimal orderPrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getAmount() {
        return amount;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }
}